<?php

use App\Http\Controllers\MathController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main landing page
Route::get('/', function () {
	return view('dashboard');
});
// Math controller does not accept GET - redirect to the dashboard
Route::get('/math', function () {
	return redirect('/');
});
// It is Laravel's philosophy to explicitly define each route, rather than "auto-routing" like Zend or CodeIgniter
Route::post('/math/sum_of_squares', [MathController::class, 'sum_of_squares']);
Route::post('/math/square_of_the_sum', [MathController::class, 'square_of_the_sum']);
Route::post('/math/difference', [MathController::class, 'difference']);
Route::post('/math/pythagorean_triplet', [MathController::class, 'pythagorean_triplet']);
Route::post('/math/sequence_product', [MathController::class, 'sequence_product']);
