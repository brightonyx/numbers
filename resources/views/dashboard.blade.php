<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Industry Arts Code Challenge</title>
	<meta name="description" content="Homework task v2.2">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="https://industry-arts.com/wp-content/uploads/2020/08/cropped-industryarts-favicon-512-32x32.png">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mini.css/3.0.1/mini-default.min.css">
	<style>
		.container {
			width: 720px;
		}
		input {
			width: 99%;
		}
		table {
			overflow: hidden !important;
		    flex: 1 0 0% !important;
		}
		.collapse > :checked + label + div {
			max-height: 600px;
		}
		.inline-block {
			display: block !important;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>Math Problems</h1>
		<div class="collapse">
			<input type="radio" id="accordion-section-one" checked aria-hidden="true" name="accordion">
			<label for="accordion-section-one" aria-hidden="true">Single number</label>
			<div>
				<p>Using this form, you will be able to get:</p>
				<ul>
					<li>the sum of the squares of the first n natural numbers</li>
					<li>the square of the sum of the same first n natural numbers, where n is guaranteed to be no greater than 100</li>
					<li>the difference of the sums above</li>
				</ul>
				<form id="single-numbers-form" method="POST">
					<fieldset>
						<legend>Please enter a natural number and select the type of operation to perform</legend>
						<div>
							<input placeholder="Number" name="n" type="number" min="1" max="100">
						</div>
					</fieldset>
					<br>
					<div>
						<button id="sum-of-squares" data-url="/math/sum_of_squares" class="primary" type="submit">Sum of squares</button>
						<button id="square-of-the-sums" data-url="/math/square_of_the_sum" class="primary" type="submit">Square of the sum</button>
						<button id="difference-of-sums" data-url="/math/difference" class="primary" type="submit">Difference of the sums</button>
					</div>
				</form>
			</div>
			<input type="radio" id="accordion-section-two" aria-hidden="true" name="accordion">
			<label for="accordion-section-two" aria-hidden="true">Multiple numbers</label>
			<div>
				<p>Using this form, you will be able to determine:</p>
				<ul>
					<li>if a sequence of three natural numbers (a,b and c) are a Pythagorean triplet</li>
					<li>the product of the sequence of these three numbers where abc = n, where c is guaranteed to be no greater than 1000</li>
				</ul>
				<form id="multiple-numbers-form" method="POST">
					<fieldset>
						<legend>Please enter three numbers and select the type of operation to perform</legend>
						<div>
							<input placeholder="First number" name="a" value="">
						</div>
						<div>
							<input placeholder="Second number" name="b" value="">
						</div>
						<div>
							<input placeholder="Third number" name="c" value="">
						</div>
					</fieldset>
					<br>
					<div>
						<button id="pythagorean-triplet" data-url="/math/pythagorean_triplet" class="primary" type="submit">Pythagorean triplet</button>
						<button id="product-of-sequence" data-url="/math/sequence_product" class="primary" type="submit">Product of sequence</button>
					</div>
				</form>
			</div>
		</div>
		<p id="alert" style="display: none">
			<mark class="inline-block tertiary"></mark>
		</p>
	</div>
	<div class="container">
		<br>
		<div id="results" style="display: none">
			<div class="row">
				<table class="horizontal">
					<caption>Result</caption>
					<thead>
						<tr>
							<th>Date</th>
							<th>Value</th>
							<th>Number</th>
							<th>Occurrences</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
	<script type="application/javascript" src="https://zeptojs.com/zepto.min.js"></script>
	<script type="application/javascript" src="/js/common.js"></script>
	<script>
		Zepto(function ($) {
			// Make sure the results remain hidden
			$('body').on('click', function(e) {
				$('#results').hide();
			});
			// Bind the ajax action to both forms
			$('#single-numbers-form, #multiple-numbers-form').on('submit', function (e) {
				// Disable any default form actions
				e.preventDefault();
				// Disable the submit button to avoid duplicate requests
				let button = $(e.submitter).attr('disabled', true);
				// Get the endpoint URL
				let url = button.data('url');
				// Send a request to the backend
				$(this).post(url, function (response) {
					// Re-enable the button back
					button.removeAttr('disabled');
					// Generate the "card" with the fours columns from the response
					let body = '<tr><td data-label="Date">' + response.data.datetime + '</td>';
					body += '<td data-label="Value">' + response.data.value + '</td>';
					body += '<td data-label="Number">' + response.data.number + '</td>';
					body += '<td data-label="Occurrences">' + response.data.occurrences + '</td></tr>';
					// Append the HTML back to the parent div
					$('#results .row tbody').html(body);
					$('#results').show();
				});
			});
		});
	</script>
</body>
</html>
