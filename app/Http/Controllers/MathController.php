<?php

namespace App\Http\Controllers;

use Helpers\Util;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Routing\Controller as BaseController;

class MathController extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	 * Computes a sum of the squares of the first n natural numbers
	 * Example: the sum of the squares of the first ten natural numbers is 1^2 + 2^2 + ... + 10^2 = 385
	 *
	 */
	public function sum_of_squares(Request $request) {
		$input = json_decode($request->getContent());
		// Perform some basic validation
		if ($number = (int)$input->n) {
			// Check if the number is natural
			if (filter_var($number, FILTER_VALIDATE_INT)) {
				// Set the redis key to the current method name. Could be a constant set in this class or elsewhere
				$key = __FUNCTION__;
				return Util::json_data($this->generate_response($key . '_' . $number, $number, Util::sum_squares($number)));
			}
			return Util::json_error('Input is not a natural number');
		}
		// Input is invalid
		return Util::json_error('Please enter a valid natural number');
	}

	/**
	 * Computes a square of the sum of the same first n natural numbers, where n is guaranteed to be no greater than 100
	 * Example: the square of the sum of the first ten natural numbers is (1 + 2 + ... + 10)^2 = 552 = 3025
	 *
	 */
	public function square_of_the_sum(Request $request) {
		$input = json_decode($request->getContent());
		// Perform some basic validation
		if ($number = (int)$input->n and $number > 0 and $number <= 100) {
			// Check if the number is natural
			if (filter_var($number, FILTER_VALIDATE_INT)) {
				return Util::json_data($this->generate_response(__FUNCTION__ . '_' . $number, $number, Util::square_of_sum($number)));
			}
			return Util::json_error('Input is not a natural number');
		}
		// Input is invalid
		return Util::json_error('Please enter a valid natural number');
	}

	/**
	 * Computes a difference between square_of_the_sum() and sum_of_squares()
	 *
	 */
	public function difference(Request $request) {
		$input = json_decode($request->getContent());
		// Perform some basic validation
		if ($number = (int)$input->n and $number > 0 and $number <= 100) {
			// Check if the number is natural
			if (filter_var($number, FILTER_VALIDATE_INT)) {
				return Util::json_data($this->generate_response(__FUNCTION__ . '_' . $number, $number, Util::square_of_sum($number) - Util::sum_squares($number)));
			}
			return Util::json_error('Input is not a natural number');
		}
		// Input is invalid
		return Util::json_error('Please enter a valid natural number');
	}

	/**
	 * Determines if a sequence of three natural numbers (a,b and c) is a Pythagorean triplet
	 * Currently, it follows the same output rules as the methods above. However, it can easily be changed to respond with a boolean only
	 *
	 */
	public function pythagorean_triplet(Request $request) {
		$input = json_decode($request->getContent());
		// Perform some basic validation
		if ($a = (int)$input->a and $b = (int)$input->b and $c = (int)$input->c) {
			// Check if the numbers are natural
			if (filter_var($a, FILTER_VALIDATE_INT)) {
				if (filter_var($b, FILTER_VALIDATE_INT)) {
					if (filter_var($c, FILTER_VALIDATE_INT)) {
						$number = "$a, $b, $c";
						$is_pythogorean = Util::is_pythogorean($a, $b, $c) ? 'Pythagorean triplet!' : 'Not a Pythagorean triplet';
						return Util::json_data($this->generate_response(__FUNCTION__ . '_' . $number, $number, $is_pythogorean));
					} else {
						return Util::json_error('Input C is not a natural number');
					}
				} else {
					return Util::json_error('Input B is not a natural number');
				}
			} else {
				return Util::json_error('Input A is not a natural number');
			}
		}
		// Input is invalid
		return Util::json_error('Please enter three valid natural numbers');
	}

	/**
	 * Determines if a product of the sequence of these three numbers where abc = n, where c is guaranteed to be no greater than 1000
	 * Currently, it follows the same output rules as the methods above. However, it can easily be changed to respond with a boolean only
	 *
	 */
	public function sequence_product(Request $request) {
		$input = json_decode($request->getContent());
		// Perform some basic validation
		if ($a = (int)$input->a and $b = (int)$input->b and $c = (int)$input->c and $c <= 1000) {
			// Check if the numbers are natural
			if (filter_var($a, FILTER_VALIDATE_INT)) {
				if (filter_var($b, FILTER_VALIDATE_INT)) {
					if (filter_var($c, FILTER_VALIDATE_INT)) {
						$number = "$a, $b, $c";
						return Util::json_data($this->generate_response(__FUNCTION__ . '_' . $number, $number, $a * $b * $c));
					} else {
						return Util::json_error('Input C is not a natural number');
					}
				} else {
					return Util::json_error('Input B is not a natural number');
				}
			} else {
				return Util::json_error('Input A is not a natural number');
			}
		}
		// Input is invalid
		return Util::json_error('Please enter three valid natural numbers');
	}

	/**
	 * Prepares an output object
	 *
	 * @param string $key
	 * @param int|string $number
	 * @param int|string $result
	 * @return array
	 */
	private function generate_response(string $key, $number, $result): array {
		// Define the number of times n has been requested. Check to see if it was already cached
		$counter = Redis::exists($key) ? Redis::get($key) : 0;
		// Increment the counter since this is a valid request
		$counter++;
		// Save back to cache
		Redis::set($key, $counter);
		return [
			'datetime'    => date('Y-m-d H:i:s'),
			'value'       => $result,
			'number'      => $number,
			'occurrences' => $counter,
		];
	}
}
