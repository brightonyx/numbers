<?php
/**
 * Utility class that includes various helper functions, wrappers and tools to aid the main app development
 *
 * @author Paul Brighton
 * @date October 2021
 * @since 1.0
 */

namespace Helpers;

use App\Models\Response;
use Illuminate\Http\JsonResponse;

class Util {
	/**
	 * Acts as a wrapper to send json error response back to the client
	 *
	 * @param string $message Error message
	 */
	public static function json_error(string $message): JsonResponse {
		$response          = new Response();
		$response->error   = $message;
		$response->success = false;
		return self::send_json($response);
	}

	/**
	 * Acts as a wrapper to send json success response back to the client
	 *
	 * @param string $message Success message
	 */
	public static function json_success(string $message): JsonResponse {
		$response          = new Response();
		$response->message = $message;
		return self::send_json($response);
	}

	/**
	 * Acts as a wrapper to send json data response back to the client
	 *
	 * @param object|array $data
	 * @param string|null $message
	 */
	public static function json_data($data, string $message = null): JsonResponse {
		$response          = new Response();
		$response->data    = $data;
		$response->message = $message;
		return self::send_json($response);
	}

	/**
	 * Main logic that sends off json response data and headers
	 *
	 * @param Response $response
	 * @return JsonResponse
	 */
	public static function send_json(Response $response): JsonResponse {
		return response()->json($response);
	}

	/**
	 * Gets a sum of the squares
	 *
	 * @param int $number
	 * @return int
	 */
	public static function sum_squares(int $number): int {
		$sum = 0;
		// Get the sum of the squares
		for ($i = 1; $i <= $number; $i++) {
			$sum += $i * $i;
		}
		return $sum;
	}

	/**
	 * Gets a square of the sum
	 *
	 * @param int $number
	 * @return int
	 */
	public static function square_of_sum(int $number): int {
		return ($number * ($number + 1) / 2) ** 2;
	}

	/**
	 * Determines if a set of natural numbers is a Pythagorean triplet
	 *
	 * @param int $a
	 * @param int $b
	 * @param int $c
	 * @return bool
	 */
	public static function is_pythogorean(int $a, int $b, int $c): bool {
		$a_squared = $a * $a;
		$b_squared = $b * $b;
		$c_squared = $c * $c;
		return $c_squared === ($a_squared + $b_squared);
	}
}
