<?php
/**
 * Response message entity. Contains details about response variables passed back from our API.
 *
 * @author Paul Brighton
 * @date October 2021
 * @since 1.0
 */

namespace App\Models;

class Response {

	/** @var bool $success True by default, changes to false automatically if any errors are detected in the $errors array */
	public $success;

	/** @var string $error Most recent server error. In production of course we will use an array of Error entities */
	public $error;

	/** @var string $message Final message to display to the client */
	public $message;

	/** @var array|object $data Used to output lists and other data elements back to the client */
	public $data;

	/** @var string $requested_at Log the date when this request was made */
	public $requested_at;

	/**
	 * Class constructor
	 *
	 */
	public function __construct() {
		// Treat this is a successful response by default
		$this->success    = true;
		$this->requested_at = date('Y-m-d H:i:s');
	}
}
