# The Problem #

Please design and develop a service that I can query that will yield the difference between:

- the sum of the squares of the first n natural numbers
- the square of the sum of the same first n natural numbers, where n is guaranteed to be no greater than 100.

Example:

The sum of the squares of the first ten natural numbers is:

1^2 + 2^2 + ... + 10^2 = 385

The square of the sum of the first ten natural numbers is:

(1 + 2 + ... + 10)^2 = 55^2 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Requirements:

- Use Laravel.
- I should be able to install your service locally. Please include a README with instructions for launching it locally.
- I should be able to query your service at the following (or similar) endpoint:

  /difference?number=n where n is an integer greater than 0 and less than or equal to 100.
- Your service should emit a JSON object of the following structure:
```
{
  "datetime":current_datetime,
  "value":solution,
  "number":n,
  "occurrences":occurrences // the number of times n has been requested
}
```

For persistence, you can use Postgres, MySQL, sqlite3 or Redis.

Optionals (Any or all of the below)

- Create an application (any front end framework or just jquery) based on the above backend service that should display a list of the above values in the four columns described above.
- Your UI should have a form to enter the number that you wish to query.
- Assume this is only the first of many such similar requests. For example, as a team we have decided that users really want to know the answer you may need to develop a service that also asks the following:
- Please design and develop a service that I can query that will yield
  - if a sequence of three natural numbers (a,b and c) are a Pythagorean triplet
  - the product of the sequence of these three numbers where abc = n, where c is guaranteed to be no greater than 1000.
  - Construct your application in such a way that you can easily scale to meet these additional product needs.
  - Use PHP 7.4+ style type-hinting
  - Unit tests are appreciated

# The Solution #

### Project Comments ###

* This app is built on Laravel 8 with Redis as the persistent storage mechanism
* It works with PHP7.3 and higher
* Both `phpredis` and `predis` drivers are supported. Defaults to `predis`
* On the frontend it uses [mini.css](https://minicss.org/) for minimal styling and [zapto.js](https://zeptojs.com/) for minimal jquery equivalent
* There are two accordions on the dashboard for the main math problems, and optional
* All backend requests and responses are done via JSON only
* A [custom prototype](https://bitbucket.org/brightonyx/numbers/src/master/public/js/common.js) for submitting forms can be invoked with `$('#form').post({endpoint});`
* Basic validation happens in two places: HTML5 element types, and backend filters
* Due to the lack of time, unit tests are not included

### Build Instructions ###

* Clone the repo
* Configure your `.env` file to reflect the server hostname and redis connection settings
* Do a `composer update`
* Navigate to the server URL you have set in the previous step
* All backend calls are done via AJAX to a `/math` endpoint